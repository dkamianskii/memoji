function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}


//Game initialization
function gameInit(contentSequence){
  //Selecting game-board element
  gameBoard = document.querySelector('.game-board');

  // Creating independent array of content elements and double it
  contentArr = contentSequence.slice();
  contentSequence.forEach((elt) => {contentArr.push(elt)});

//Creating and inserting card object with randomly chosen content element
  let number = contentArr.length;
  while(contentArr.length !== 0){
    idx = getRandomInt(0,contentArr.length);
    gameBoard.appendChild(cardCreate(contentArr[idx], number));
    number-=1;
    contentArr.splice(idx,1);
  }

  const cardClick = function(event){
    event.stopPropagation();
    if(event.target.tagName === 'DIV'){
      let card = document.querySelector('.card[data-number="'+event.target.dataset.number+'"]');
      if(card.classList.contains("card_turned-front")){
        return;
      }

      if(card.classList.contains("card_default")){
        card.classList.remove("card_default");
        card.classList.add("card_turned-front");
      }else if(card.classList.contains("card_turned-back")){
        card.classList.remove("card_turned-back");
        card.classList.add("card_turned-front");
      }

      let secondCard = document.querySelector(".card_chosen");
      // console.log(secondCard);
      if(secondCard === null){
        card.classList.add("card_chosen");
        let wrongCards = document.querySelectorAll(".card_chosen_wrong");
        if(wrongCards.length !== 0){
          wrongCards.forEach((elt)=>{
            elt.classList.remove("card_chosen_wrong");
            elt.classList.remove("card_turned-front");
            elt.classList.add("card_turned-back");
          });
        }
      }else{
        if(card.textContent === secondCard.textContent){
          secondCard.classList.remove("card_chosen");
          card.classList.add("card_chosen_right");
          secondCard.classList.add("card_chosen_right");
        }else{
          secondCard.classList.remove("card_chosen");
          card.classList.add("card_chosen_wrong");
          secondCard.classList.add("card_chosen_wrong");
        }
      }
    }
  }

 gameBoard.addEventListener('click', cardClick);
}
