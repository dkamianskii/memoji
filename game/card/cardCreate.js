function cardCreate(content, number){
  let card = document.createElement('li');
  card.className = "card card_default";
  card.setAttribute("data-number",number);

  let card__side_front = document.createElement('div');
  card__side_front.className = "card__side card__side_front";
  card__side_front.textContent = content;
  card__side_front.setAttribute("data-number",number);
  let card__side_back = document.createElement('div');
  card__side_back.className = "card__side card__side_back";
  card__side_back.setAttribute("data-number",number);

  card.appendChild(card__side_front);
  card.appendChild(card__side_back);

  return card;
}
